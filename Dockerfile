FROM nginx:1.15.8-alpine
# uses alpine 3.8

MAINTAINER Exporo Development Team <devteam@exporo.com>


# Environment variables
ENV BUILD_PACKAGES="wget tar make gcc g++ zlib-dev libressl-dev pcre-dev fcgi-dev jpeg-dev libmcrypt-dev bzip2-dev curl-dev libpng-dev libxslt-dev postgresql-dev perl-dev file acl-dev libedit-dev" \
    ESSENTIAL_PACKAGES="libressl pcre zlib supervisor sed re2c m4 ca-certificates py-pip icu-dev" \
    UTILITY_PACKAGES="bash vim" \
    PHP_VER="7.2.14" \
    GETTEXT_VER="0.19.8.1" \
    BISON_VER="3.0.5" \
    AWS_CLI_VER="1.16.88"

# Configure essential and utility packages
RUN apk update && \
    apk --no-cache --progress add $ESSENTIAL_PACKAGES $UTILITY_PACKAGES && \
    mkdir -p /run/nginx/ && \
    chmod ugo+w /run/nginx/ && \
    rm -f /etc/nginx/nginx.conf && \
    mkdir -p /etc/nginx/conf.d && \
    mkdir -p /etc/nginx/ssl/ && \
    mkdir -p /var/www/html/ && \
    chmod -R 775 /var/www/ && \
    chown -R nginx:nginx /var/www/ && \
    pip install --upgrade pip && \
    pip install supervisor-stdout && \
    rm -rf /var/cache/apk/*

# Build and configure php/php-fpm
RUN apk --no-cache --progress add $BUILD_PACKAGES && \
    wget http://ftp.gnu.org/pub/gnu/gettext/gettext-${GETTEXT_VER}.tar.gz && \
    tar -zxvf gettext-${GETTEXT_VER}.tar.gz && \
    cd gettext-${GETTEXT_VER} && \
    ./configure && \
    make && \
    make install && \
    make clean && \
    cd .. && \
    rm -f gettext-${GETTEXT_VER}.tar.gz && \
    rm -rf gettext-${GETTEXT_VER} && \
    wget http://ftp.gnu.org/gnu/bison/bison-${BISON_VER}.tar.gz && \
    tar -zxvf bison-${BISON_VER}.tar.gz && \
    cd bison-${BISON_VER} && \
    ./configure && \
    make && \
    make install && \
    make clean && \
    cd .. && \
    rm -f bison-${BISON_VER}.tar.gz && \
    rm -rf bison-${BISON_VER} && \
    wget http://de2.php.net/get/php-${PHP_VER}.tar.gz/from/this/mirror -O php-${PHP_VER}.tar.gz && \
    tar -zxvf php-${PHP_VER}.tar.gz && \
    cd php-${PHP_VER} && \
    ./configure \
    --prefix=/usr \
    --with-config-file-path=/etc \
    --with-config-file-scan-dir=/etc/php.d \
    --disable-cgi \
    --enable-mbstring \
    --enable-mysqlnd \
    --enable-soap \
    --enable-calendar \
    --enable-inline-optimization \
    --enable-sockets \
    --enable-sysvsem \
    --enable-sysvshm \
    --enable-pcntl \
    --enable-mbregex \
    --enable-exif \
    --enable-bcmath \
    --enable-zip \
    --enable-ftp \
    --enable-opcache \
    --enable-intl \
    --enable-fpm \
    --enable-gd-native-ttf \
    --with-pdo-pgsql \
    --with-libedit \
    --with-libxml-dir=/usr \
    --with-curl \
    --with-mcrypt \
    --with-zlib \
    --with-gd \
    --with-pgsql \
    --with-bz2 \
    --with-zlib \
    --with-mhash \
    --with-pcre-regex \
    --with-pdo-mysql \
    --with-jpeg-dir=/usr \
    --with-png-dir=/usr \
    --with-openssl \
    --with-fpm-user=nginx \
    --with-fpm-group=nginx \
    --with-libdir=/usr/lib \
    --with-gettext \
    --with-xmlrpc \
    --with-xsl \
    --with-pear \
    --with-mysqli && \
    make && \
    make install && \
    make clean && \
    cd .. && \
    rm -f php-${PHP_VER}.tar.gz && \
    rm -rf php-${PHP_VER} && \
    mkdir -p /etc/php.d && \
    chmod 755 /etc/php.d && \
    mkdir -p /usr/lib/php/modules && \
    ln -s /usr/lib/php/extensions/no-debug-non-zts-20160303/opcache.so /usr/lib/php/modules/opcache.so && \
    rm -rf /var/cache/apk/*

# Install AWS CLI for bootstrapping apps from the inside
RUN apk --no-cache add py-pip py-setuptools ca-certificates groff less && \
    pip --no-cache-dir install awscli==${AWS_CLI_VER} python-magic && \
    apk -v --purge del py-pip && \
    rm -rf /var/cache/apk/*